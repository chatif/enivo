#ifndef INC_UTILS_H_
#define INC_UTILS_H_

#include "main.h"

#define ADDR_A 0b00110010
#define ADDR_M 0b00111100
#define WHO_AM_I_A 0x0F
#define WHO_AM_I_M 0x4F
#define CTRL_REG1_A 0x20
#define CFG_REG_A_M 0x60
#define OUT_X_L_A 0x28
#define OUTX_L_REG_M 0x68
#define CAL_LIN 6

typedef struct COORD
{
	double x, y, z;
} Coord;

typedef struct ANGLE
{
	double theta, psi, phi, delta;
} Angle;

float sq(float a);
void angle(Coord a, Coord m, Angle *angles);
Coord coordSum(Coord a, Coord b);
Coord coordBlank();
Coord coordDivide(Coord a, double d);
Angle angleDivide(Angle angle, double d);
Angle angleMulti(Angle angle, double d);
#endif /* INC_UTILS_H_ */
