#ifndef INC_SENSORS_H_
#define INC_SENSORS_H_

#include <utils.h>
#include "main.h"

void configA();
void configM();
void testRead();
void readA(Coord *a);
void readM(Coord *m);
void whoAmITest();
void average(Coord *a, Coord *m, int n);
void calibrate();

#endif /* INC_SENSORS_H_ */
