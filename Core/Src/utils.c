#include <math.h>
#include <utils.h>

float sq(float a)
{
	return a * a;
}

void angle(Coord a, Coord m, Angle *angles)
{
	float num, den;

	angles->theta = atan(a.y / a.x);

	angles->psi = atan(-a.z / sqrt(sq(a.x) + sq(a.y)));

	angles->phi = atan((m.x * sin(angles->theta) - m.y * cos(angles->theta)) / (m.z * cos(angles->psi) + m.y * sin(angles->theta) * sin(angles->psi) + m.x * cos(angles->theta) * sin(angles->psi)));

	num = sqrt(sq(m.y * a.z - m.z * a.y) + sq(m.z * a.x - m.x * a.z) + sq(m.x * a.y - m.y * a.x));

	den = sqrt(sq(m.x) + sq(m.y) + sq(m.z)) * sqrt(sq(a.x) + sq(a.y) + sq(a.z));

	angles->delta = acos(num / den);

	*angles = angleMulti(*angles, 180. / M_PI);
}

Coord coordSum(Coord a, Coord b)
{
	Coord c;

	c.x = a.x + b.x;
	c.y = a.y + b.y;
	c.z = a.z + b.z;

	return c;
}

Coord coordBlank()
{
	Coord a =
	{ 0, 0, 0 };

	return a;
}

Coord coordDivide(Coord a, double d)
{
	a.x /= d;
	a.y /= d;
	a.z /= d;

	return a;
}

Angle angleDivide(Angle angle, double d)
{
	angle.delta /= d;
	angle.phi /= d;
	angle.psi /= d;
	angle.theta /= d;

	return angle;
}

Angle angleMulti(Angle angle, double d)
{
	angle.delta *= d;
	angle.phi *= d;
	angle.psi *= d;
	angle.theta *= d;

	return angle;
}

