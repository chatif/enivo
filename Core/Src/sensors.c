#include <stdio.h>
#include <i2c.h>
#include <sensors.h>
#include <matrice.h>

Coord calibInput[CAL_LIN] =
{
{ +0.99, +0.01, +0.04 },
{ -0.98, -0.01, -0.10 },
{ +0.00, +0.97, -0.05 },
{ +0.23, -0.97, -0.07 },
{ +0.00, -0.01, +1.01 },
{ +0.03, -0.03, -1.05 } };

Coord aOfs =
{ 0, 0, 0 };

Coord mOfs =
{ 0, 0, 0 };

double caliMatrix[3][3] =
{
{ 1, 0, 0 },
{ 0, 1, 0 },
{ 0, 0, 1 } };
;

void configA()
{
	uint8_t CtrlReg1[] =
	{ CTRL_REG1_A + (1 << 7), 0b10010111, 0, 0, 0, 0, 0 };

	HAL_I2C_Master_Transmit(&hi2c1, ADDR_A, CtrlReg1, 7, 100);
}

void configM()
{
	uint8_t CfgRegA[] =
	{ CFG_REG_A_M + (1 << 7), 0b10001100, 0b00000010, 0b00000000 };

	HAL_I2C_Master_Transmit(&hi2c1, ADDR_M, CfgRegA, 4, 100);
}

void testRead()
{
	uint8_t sData[1], rData[7];
	uint16_t sSize, rSize;

	sData[0] = CTRL_REG1_A + (1 << 7);
	sSize = 1;
	rSize = 7;

	for (int i = 0; i < rSize; i++)
		rData[i] = 0;

	HAL_I2C_Master_Transmit(&hi2c1, ADDR_A, sData, sSize, 100);
	HAL_I2C_Master_Receive(&hi2c1, ADDR_A, rData, rSize, 100);

	for (int i = 0; i < rSize; i++)
		printf("CTRL_REG%d_A : %x\r\n", i + 1, (int) rData[i]);

	sData[0] = CFG_REG_A_M + (1 << 7);
	sSize = 1;
	rSize = 3;

	for (int i = 0; i < rSize; i++)
		rData[i] = 0;

	HAL_I2C_Master_Transmit(&hi2c1, ADDR_M, sData, sSize, 100);
	HAL_I2C_Master_Receive(&hi2c1, ADDR_M, rData, rSize, 100);

	for (int i = 0; i < rSize; i++)
		printf("CFG_REG_%c_M : %x\r\n", i + 'A', (int) rData[i]);
}

void readA(Coord *a)
{
	uint8_t sData[1], rData[6];
	uint16_t sSize, rSize;

	int16_t outX, outY, outZ;
	Coord accNo;

	sData[0] = OUT_X_L_A + (1 << 7);
	sSize = 1;
	rSize = 6;

	for (int i = 0; i < rSize; i++)
		rData[i] = 0;

	HAL_I2C_Master_Transmit(&hi2c1, ADDR_A, sData, sSize, 100);
	HAL_I2C_Master_Receive(&hi2c1, ADDR_A, rData, rSize, 100);

	outX = (((uint16_t) rData[1]) << 8) + rData[0];
	outY = (((uint16_t) rData[3]) << 8) + rData[2];
	outZ = (((uint16_t) rData[5]) << 8) + rData[4];

	outX = outX >> 6;
	outY = outY >> 6;
	outZ = outZ >> 6;

	accNo.x = outX / 256.;
	accNo.y = outY / 256.;
	accNo.z = outZ / 256.;

	a->x = caliMatrix[0][0] * (accNo.x - aOfs.x) + caliMatrix[0][1] * (accNo.y - aOfs.y) + caliMatrix[0][2] * (accNo.z - aOfs.z);
	a->y = caliMatrix[1][0] * (accNo.x - aOfs.x) + caliMatrix[1][1] * (accNo.y - aOfs.y) + caliMatrix[1][2] * (accNo.z - aOfs.z);
	a->z = caliMatrix[2][0] * (accNo.x - aOfs.x) + caliMatrix[2][1] * (accNo.y - aOfs.y) + caliMatrix[2][2] * (accNo.z - aOfs.z);
}

void readM(Coord *m)
{
	uint8_t sData[1], rData[6];
	uint16_t sSize, rSize;

	int16_t outX, outY, outZ;

	sData[0] = OUTX_L_REG_M + (1 << 7);
	sSize = 1;
	rSize = 6;

	for (int i = 0; i < rSize; i++)
		rData[i] = 0;

	HAL_I2C_Master_Transmit(&hi2c1, ADDR_M, sData, sSize, 100);
	HAL_I2C_Master_Receive(&hi2c1, ADDR_M, rData, rSize, 100);

	outX = (((uint16_t) rData[1]) << 8) + rData[0];
	outY = (((uint16_t) rData[3]) << 8) + rData[2];
	outZ = (((uint16_t) rData[5]) << 8) + rData[4];

	m->x = outX * 100 / 65535. + mOfs.x;
	m->y = outY * 100 / 65535. + mOfs.y;
	m->z = outZ * 100 / 65535. + mOfs.z;
}

void whoAmITest()
{
	uint8_t sData[] =
	{ WHO_AM_I_M };
	uint16_t sSize = 1;

	uint8_t rData[1];
	uint16_t rSize = 1;

	HAL_StatusTypeDef sStatus, rStatus;

	for (int i = 0; i < rSize; i++)
		rData[i] = 0;

	sStatus = HAL_I2C_Master_Transmit(&hi2c1, ADDR_M, sData, sSize, 100);
	rStatus = HAL_I2C_Master_Receive(&hi2c1, ADDR_M, rData, rSize, 100);

	printf("Send : %x\r\n", sStatus);
	printf("Receive : %x\r\n", rStatus);

	for (int i = 0; i < rSize; i++)
		printf("Data%d : %x\r\n", i, (int) rData[i]);
}

void average(Coord *a, Coord *m, int n)
{
	int i;
	Coord mesA, mesM;

	a->x = a->y = a->z = 0;
	m->x = m->y = m->z = 0;

	for (i = 0; i < n; i++)
	{
		readA(&mesA);
		readM(&mesM);

		a->x += mesA.x;
		a->y += mesA.y;
		a->z += mesA.z;

		m->x += mesM.x;
		m->y += mesM.y;
		m->z += mesM.z;
	}

	a->x /= n;
	a->y /= n;
	a->z /= n;

	m->x /= n;
	m->y /= n;
	m->z /= n;
}

void calibrate()
{
	int i, j;
	Coord a, m;
	double matrix[3][3];

	for (i = 0; i < CAL_LIN; i++)
	{
		while (HAL_GPIO_ReadPin(B1_GPIO_Port, B1_Pin))
		{
			average(&a, &m, 50);

			printf("\033[2J\033[0;0H");
			printf("------------------------- Calibration acceléromètre-------------------------\r\n\r\n");
			printf("Position : ");

			switch (i)
			{
				case 0:
					printf("X = g");
					break;
				case 1:
					printf("X = -g");
					break;
				case 2:
					printf("Y = g");
					break;
				case 3:
					printf("Y = -g");
					break;
				case 4:
					printf("Z = g");
					break;
				case 5:
					printf("Z = -g");
					break;
			}
			printf("\r\n\r\n");
			printf("Accelerometre : X: %.3fg\tY: %.3fg\tZ: %.3fg\tmoy : %.3fg\r\n", a.x, a.y, a.z, sqrt(a.x * a.x + a.y * a.y + a.z * a.z));

			for (j = 0; j < i; j++)
				printf("| %5.2f | %5.2f | %5.2f |\r\n", calibInput[j].x, calibInput[j].y, calibInput[j].z);

			printf("\r\n");
		}

		printf("\r\n\r\nRecuperation de la calibration...\r\n");

		average(&a, &m, 1000);
		calibInput[i] = a;

		while (!HAL_GPIO_ReadPin(B1_GPIO_Port, B1_Pin));
	}

	aOfs = coordBlank();

	for (i = 0; i < CAL_LIN; i++)
		aOfs = coordSum(aOfs, calibInput[i]);

	aOfs = coordDivide(aOfs, CAL_LIN);

	printf("\033[2J\033[0;0H");
	for (j = 0; j < CAL_LIN; j++)
		printf("| %5.2f | %5.2f | %5.2f |\r\n", calibInput[j].x, calibInput[j].y, calibInput[j].z);

	printf("\r\n| %5.2f | %5.2f | %5.2f |\r\n\r\n", aOfs.x, aOfs.y, aOfs.z);

	for (j = 0; j < 3; j++)
		matrix[0][j] = (calibInput[2 * j].x - calibInput[2 * j + 1].x) / 2;

	for (j = 0; j < 3; j++)
		matrix[1][j] = (calibInput[2 * j].y - calibInput[2 * j + 1].y) / 2;

	for (j = 0; j < 3; j++)
		matrix[2][j] = (calibInput[2 * j].z - calibInput[2 * j + 1].z) / 2;

	for (i = 0; i < 3; i++)
	{
		for (j = 0; j < 3; j++)
			printf("| %5.2f ", matrix[i][j]);

		printf("|\r\n\r\n");
	}

	inverse_matrice((double*) matrix, 3, (double*) caliMatrix);

	for (i = 0; i < 3; i++)
	{
		for (j = 0; j < 3; j++)
			printf("| %5.2f ", caliMatrix[i][j]);

		printf("|\r\n\r\n");
	}
}

void traceM()
{
	Coord a, m;

	average(&a, &m, 40);

	printf("%f;%f\r\n", m.x, m.y);
}
